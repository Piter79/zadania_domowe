# Definicja potrzebnych informacji: Imię odbiorcy, Rok urodzenia, spersonalizowana wiadomość, imię nadawcy


print("Witamy w programie do tworzenia spersonalizowanych kartek urodzinowych!\n")

from datetime import datetime

def oblicz_wiek(data_urodzenia):
    dzisiaj = datetime.now()
    wiek = dzisiaj.year - data_urodzenia.year - ((dzisiaj.month, dzisiaj.day) < (data_urodzenia.month, data_urodzenia.day))
    return wiek

# Pobranie pełnej daty urodzenia od użytkownika
imię = input("Podaj imię odbiorcy:")
rok = int(input("Podaj rok urodzenia odbiorcy: "))
miesiac = int(input("Podaj miesiąc urodzenia odbiorcy: "))
dzien = int(input("Podaj dzień urodzenia odbiorcy: "))
zyczenia = input("Wpisz Twoje życzenia urodzinowe:")
imie2 = input("Podaj Twoje imię:")

print()
print()
print()




data_urodzenia = datetime(rok, miesiac, dzien)
wiek = oblicz_wiek(data_urodzenia)
print(f"{imię} wszystkiego najlepszego z okazji {wiek} urodzin!\n")
print(f"{zyczenia}\n")
print(f"Życzy {imie2}")