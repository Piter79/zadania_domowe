'''
Napisz program do obsługi ładowarki paczek. Po uruchomieniu, aplikacja pyta ile produktów chcesz wysłać, a następnie wymaga podania wagi dla każdego z nich.
(Zostało poprawione z ile paczek na ile produktów w danej paczce)
Na koniec działania program powinien wyświetlić w podsumowaniu:

Liczbę paczek wysłanych
Liczbę kilogramów wysłanych
Suma "pustych" - kilogramów (brak optymalnego pakowania). Liczba paczek * 20 - liczba kilogramów wysłanych
Która paczka miała najwięcej "pustych" kilogramów, jaki to był wynik

Restrykcje:

Waga elementów musi być z przedziału od 1 do 10 kg.
Każda paczka może maksymalnie zmieścić 20 kilogramów towaru.
W przypadku, jeżeli dodawany element przekroczy wagę towaru, ma zostać dodany do nowej paczki, a obecna wysłana.
W przypadku podania wagi elementu mniejszej od 1kg lub większej od 10kg, dodawanie paczek zostaje zakończone i wszystkie paczki są wysłane. Wyświetlane jest podsumowanie.

Przykład 1:

Ilość elementów: 2
Wagi elementów: 7, 9
Podsumowanie:

Wysłano 1 paczkę (7+9)
Wysłano 16 kg
Suma pustych kilogramów: 4kg
Najwięcej pustych kilogramów ma paczka 1 (4kg)

Przykład 2:

 Ilość elementów: 6
Wagi elementów: 3, 6, 5, 8, 2, 3
Podsumowanie:

Wysłano 2 paczki (3+6+5, 8+2+3)
Wysłano 27 kg
Suma pustych kilogramów: 13kg
Najwięcej pustych kilogramów ma paczka 2 (7kg)

Przykład 3:
 Ilość elementów: 8

Wagi elementów: 7, 14
 Podsumowanie:
Wysłano 1 paczkę (7)
Wysłano 7 kg
Suma pustych kilogramów: 13kg
Najwięcej pustych kilogramów ma paczka 13
'''


print("Witaj w naszej aplikacji wysyłkowej!")
print()
ilosc_produktow = int(input("Podaj liczbę produktów do wysyłki: "))

suma_produktow = 0
ilosc_paczek = 1
max_waga_paczki = 20
waga_obecnej_paczki = 0
max_wolne_miejsce = 0
numer_paczki_z_max_pustymi_kg = 1

for produkt in range(ilosc_produktow):
    waga_produktu = int(input("Podaj wagę produktu w kilogramach (1-10kg): "))

    if waga_produktu < 1 or waga_produktu > 10:
        print("Nieprawidłowa waga produktu!")
        break
    suma_produktow += waga_produktu
    if waga_produktu + waga_obecnej_paczki > max_waga_paczki:
        if max_waga_paczki - waga_obecnej_paczki > max_wolne_miejsce:
            max_wolne_miejsce = max_waga_paczki - waga_obecnej_paczki
        ilosc_paczek += 1
        waga_obecnej_paczki = waga_produktu
    else:
        waga_obecnej_paczki += waga_produktu
if max_waga_paczki - waga_obecnej_paczki > max_wolne_miejsce:
    max_wolne_miejsce = max_waga_paczki - waga_obecnej_paczki
    numer_paczki_z_max_pustymi_kg = ilosc_paczek

print()
print("Podsumowanie:")
print(f"Ilość wysłanych paczek: {ilosc_paczek}")
print(f"Ilość wysłanych kg: {suma_produktow}")
print(f"Suma pustych kilogramów: {ilosc_paczek * max_waga_paczki - suma_produktow}")
print(f"Paczka z największą liczbą pustych kilogramów to paczka nr {numer_paczki_z_max_pustymi_kg} ({max_waga_paczki - waga_obecnej_paczki} kg)")
